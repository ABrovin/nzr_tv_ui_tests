package Utils;

import java.util.HashMap;
import java.util.Map;

public class TestSettings {

    //region Constants

    public static final String localAddress = "http://localhost:3000";
    public static final String elasticbeanstalkAddress = "http://nzrtv-test.ap-southeast-2.elasticbeanstalk.com";
    public static final String siteAddress = elasticbeanstalkAddress;

    public static final int timeOut = 30000;
    public static final int veryShortTimeOut = 30000 / 6;
    public static final int shortTimeOut = timeOut / 2;
    public static final int longTimeOut = timeOut * 2;

    //endregion

    //region Accounts

    public static class Account {
        public String email;
        public String pass;
        public String name;

        public Account(String email, String pass) {
            this.email = email;
            this.pass = pass;
        }
    }

    public enum AccountTypes {
        Facebook, Google, Twitter
    }

    public static Map<AccountTypes, Account> socialAccounts;

    static {
        socialAccounts = new HashMap<>();
        socialAccounts.put(AccountTypes.Facebook, new Account("rugby.testet@gmail.com", "Password!@"));
        socialAccounts.put(AccountTypes.Google, new Account("rugby.testet@gmail.com", "Password!@"));
        socialAccounts.put(AccountTypes.Twitter, new Account("rugby.testet@gmail.com", "Password!@"));
    }

    public static Map<String, Account> accounts;
    static {
        accounts = new HashMap<>();
        accounts.put("rugby.testet@gmail.com", new Account("rugby.testet@gmail.com", "Password!@"));
        accounts.put("john@snow.co.nz", new Account("john@snow.co.nz", "Test123"));
    }

    //endregion
}


