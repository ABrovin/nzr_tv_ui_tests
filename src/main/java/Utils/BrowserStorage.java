package Utils;

import com.codeborne.selenide.WebDriverRunner;
import com.google.gson.Gson;
import org.openqa.selenium.JavascriptExecutor;

public class BrowserStorage {

    private static String getItemFromLocalStorage(String key) {
        try {
            JavascriptExecutor js = ((JavascriptExecutor) WebDriverRunner.getWebDriver());
            return (String) js.executeScript(String.format("return window.localStorage.getItem('%s');", key));
        } catch (Exception e) {
            return "";
        }
    }

    private static void clearLocalStorage() {
        ((JavascriptExecutor) WebDriverRunner.getWebDriver()).executeScript("return window.localStorage.clear();");
    }

    public static String getCurrentUser() {
        String jsonString = getItemFromLocalStorage("ajs_user_traits");
        try {
            TestSettings.Account acc = new Gson().fromJson(jsonString, TestSettings.Account.class);
            return acc.email;
        } catch (Exception e) {
            return "";
        }
    }

    public static void clear() {
        WebDriverRunner.getWebDriver().manage().deleteAllCookies();
        clearLocalStorage();
    }
}
