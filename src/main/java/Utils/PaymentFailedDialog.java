package Utils;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static Utils.TestSettings.veryShortTimeOut;
import static com.codeborne.selenide.Selenide.$;

public class PaymentFailedDialog {
    //region Elements

    public By dialogSelector = By.cssSelector("#payment-failed-modal");
    public SelenideElement dialog = $(dialogSelector);

    public SelenideElement button = dialog.$(By.cssSelector(".button[href='#']"));

    //endregion

    public boolean modalDialogExist() throws InterruptedException {
        return Wait.waitFor(() -> dialog.isDisplayed(), veryShortTimeOut);
    }

    public void clickTryAgain() {
        this.button.click();
    }
}


