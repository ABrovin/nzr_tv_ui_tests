package StepDefinitions;
import java.util.concurrent.TimeUnit;
import InteractionObjects.ContactPage;
import InteractionObjects.LoginPage;
import InteractionObjects.MainPage;
import InteractionObjects.MatchInformationPanel;
import Utils.PaymentFailedDialog;
import Utils.TestSettings;
import Utils.Wait;

import static Utils.TestSettings.accounts;
import static Utils.TestSettings.shortTimeOut;
import static Utils.TestSettings.veryShortTimeOut;

import java.util.Map;

import com.codeborne.selenide.SelenideElement;


import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.AutoCloseableSoftAssertions;
import com.codeborne.selenide.*;


public class ContactUsTestDefs {
	   LoginPage loginPage = new LoginPage();
	    MainPage mainPage = new MainPage();
	    ContactPage contactPage = new ContactPage();
	
	@When("^I go to the \"([^\"]*)\" page$")
	public void i_go_to_the_page(String arg1) throws Throwable {
	    mainPage.goToContact();        	
	    //throw new PendingException();
	}

	@Then("^I should see a \"([^\"]*)\" contact form header$")
	public void i_should_see_a_contact_form_header(String arg1) throws Throwable {
//
//	        String header = "GET IN TOUCH WITH US";
//	        String curContactHeader = contactPage.getContactHeader();
//
//	        try (AutoCloseableSoftAssertions softly = new AutoCloseableSoftAssertions()) {
//	            softly.assertThat(curFormHeader).
//	                    as("The header of contact form is wrong. Expected: \"%s\". Showed: \"%s\"", header, curContactHeader).containsIgnoringCase(header);
//	        }
	}
//TBD
	@Then("^I should see the following contact form fields:$")
	public void i_should_see_the_following_contact_form_fields(String arg1) throws Throwable {

	}
//TBD
	@Then("^the following feature buttons: \"([^\"]*)\"$")
	public void the_following_feature_buttons(String arg1) throws Throwable {

	}
	
//Scenario Outline: Submit "contact us" form
	@When("^I enter the following infos into the contact form fields$")
	public void i_enter_the_following_infos_into_the_contact_form_fields(DataTable contactFormField) throws Throwable {
		for (Map<String, String> data : contactFormField.asMaps(String.class, String.class)) {
		contactPage.fillContactForm(data.get("contact_firstname"), data.get("contact_lastname"), data.get("contact_email"), data.get("contact_subject"));
		}
	}
	
	@When("^I enter the following message into the contact message formfield:$")
	public void i_enter_the_following_message_into_the_contact_message_formfield(String contact_Comments) throws Throwable {
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Thread.sleep(500); 
		contactPage.fillContactComments(contact_Comments);
		Thread.sleep(500); 
	}

	@When("^I submit the contact form$")
	public void i_submit_the_contact_form() throws Throwable {
		contactPage.clickContactSendButton();
	}

	@Then("^I should see a success message \"([^\"]*)\"$")
	public void i_should_see_a_success_message(String expectedErrorMessage) throws Throwable {
	        try (AutoCloseableSoftAssertions softly = new AutoCloseableSoftAssertions()) {
	            String currentErrorMessage = contactPage.getAlertSuccess(); 
	            softly.assertThat(currentErrorMessage).
	                    as("Expected message: \"%s\". But was: \"%s\".", expectedErrorMessage, currentErrorMessage).isEqualToIgnoringCase(expectedErrorMessage);
	        }
	}
	
//  Scenario Outline: Submit "contact us" form fails	
	@Then("^I should see an error message for contact \"([^\"]*)\"$")
	public void i_should_see_an_error_message_for_contact(String expectedErrorMessage) throws Throwable {
//		System.out. println("current msg is: ");
//		System.out.println(currentErrorMessage);
//		
//		System.out.println("expected msg is:");
//		System.out.println(expectedErrorMessage);
		
		try (AutoCloseableSoftAssertions softly = new AutoCloseableSoftAssertions()) {
	            String currentErrorMessage = contactPage.getAlertError(); 
	            softly.assertThat(currentErrorMessage).
	                    as("Expected message: \"%s\". But was: \"%s\".", expectedErrorMessage, currentErrorMessage).isEqualToIgnoringCase(expectedErrorMessage);
	        }
	}
	
	@When("^I choose cancel$")
	public void i_choose_cancel() throws Throwable {
		contactPage.clickContactCancelButton();
	}
	
//TBD
	@Then("^I should be redirected to the \"([^\"]*)\"$")
	public void i_should_be_redirected_to_the(String arg1) throws Throwable {
	
	}



}
