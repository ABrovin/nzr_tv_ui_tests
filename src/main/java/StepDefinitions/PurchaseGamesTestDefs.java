package StepDefinitions;

import InteractionObjects.LoginPage;
import InteractionObjects.MainPage;
import InteractionObjects.MatchInformationPanel;
import Utils.PaymentFailedDialog;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.AutoCloseableSoftAssertions;

public class PurchaseGamesTestDefs {
    LoginPage loginPage = new LoginPage();
    MainPage mainPage = new MainPage();
    MatchInformationPanel matchInformationPanel = new MatchInformationPanel();
    PaymentFailedDialog paymentFailedDialog = new PaymentFailedDialog();

    @Given("^there is a published game$")
    public void there_is_a_published_game() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }

    @Given("^I am eligible to view games in my \"([^\"]*)\"$")
    public void i_am_eligible_to_view_games_in_my(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }

    @Given("^I authenticated myself with my \"([^\"]*)\"$")
    public void i_authenticated_myself_with_my(String email) throws Throwable {
        loginPage.authenticate(email);
    }

    @Given("^I visit the \"([^\"]*)\"$")
    public void i_visit_the(String arg1) throws Throwable {
        mainPage.openPage();
    }

    @Given("^I didn't purchase the game yet$")
    public void i_didn_t_purchase_the_game_yet() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        //throw new PendingException();
    }

    @Given("^I selected a game$")
    public void i_selected_a_game() throws Throwable {
        mainPage.selectGame("All Blacks v Barbarians");
    }

    @When("^I provide the following details of my \"([^\"]*)\" in the payment form: \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\"$")
    public void i_provide_the_following_details_of_my_in_the_payment_form(String card_type, String name_on_Card, String card_Number, String card_CVC, String card_Expiry) throws Throwable {
        matchInformationPanel.fillPaymentForm(card_type, name_on_Card, card_Number, card_CVC, card_Expiry);
    }

    @When("^I submit the \"([^\"]*)\"$")
    public void i_submit_the(String arg1) throws Throwable {
        matchInformationPanel.clickBuyButton();
    }

    @When("^My request was processed$")
    public void my_request_was_processed() throws Throwable {
        try (AutoCloseableSoftAssertions softly = new AutoCloseableSoftAssertions()) {
            softly.assertThat(paymentFailedDialog.modalDialogExist()).
                    as("Payment failed modal dialog should appear.").isTrue();
        }

        if (paymentFailedDialog.modalDialogExist())
            paymentFailedDialog.clickTryAgain();
    }

    @Then("^I see an \"([^\"]*)\"$")
    public void i_see_an(String expectedErrorMessage) throws Throwable {
        try (AutoCloseableSoftAssertions softly = new AutoCloseableSoftAssertions()) {
            String currentErrorMessage = matchInformationPanel.getErrorMessage();
            softly.assertThat(currentErrorMessage).
                    as("Expected message: \"%s\". But was: \"%s\".", expectedErrorMessage, currentErrorMessage).isEqualToIgnoringCase(expectedErrorMessage);
        }
    }

    @Then("^I can go back to the \"([^\"]*)\" to try again$")
    public void i_can_go_back_to_the_to_try_again(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // throw new PendingException();
    }
}
