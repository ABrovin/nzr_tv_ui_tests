package StepDefinitions;

import Utils.BrowserStorage;
import com.codeborne.selenide.WebDriverRunner;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

    @Before("@logOutBeforeTestStart")
    public void logOutBeforeTestStart() throws Throwable {
        if (WebDriverRunner.hasWebDriverStarted() && World.mainPage.isOpen())
            World.mainPage.logOut();
        else
            BrowserStorage.clear();
    }

    @After("@logOutAfterTestEnd")
    public void logOutAfterTestEnd(Scenario scenario) throws Throwable {
        if (WebDriverRunner.hasWebDriverStarted() && World.mainPage.isOpen() && scenario.getStatus().equalsIgnoreCase("passed"))
            World.mainPage.logOut();
        else
            BrowserStorage.clear();
    }
}
