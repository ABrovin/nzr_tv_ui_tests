package StepDefinitions;

import InteractionObjects.LoginPage;
import InteractionObjects.MainPage;
import Utils.Wait;
import com.codeborne.selenide.SelenideElement;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.AutoCloseableSoftAssertions;

import static Utils.TestSettings.shortTimeOut;

public class LoginTestDefs {

    String currentEmail, currentPass;

    LoginPage loginPage = new LoginPage();
    MainPage mainPage = new MainPage();

    @Given("^I have registered email address \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void i_have_registered_email_address_and_password(String arg1, String arg2) throws Throwable {
        currentEmail = arg1;
        currentPass = arg2;
    }

    @Given("^I am on the NZR ID login page$")
    public void i_am_on_the_NZR_ID_login_page() throws Throwable {
        loginPage.openPage();
    }

    @When("^I login$")
    public void i_login() throws Throwable {
        loginPage.authenticate(currentEmail, currentPass);
    }

    @Then("^I should see an error message \"([^\"]*)\"$")
    public void i_should_see_an_error_message(String arg1) throws Throwable {
        String errorMsg = "";
        if (!loginPage.getErrorPopups().isEmpty())
            for (SelenideElement element : loginPage.getErrorPopups()) {
                errorMsg = element.getAttribute("innerText");
                if (errorMsg.contains(arg1))
                    break;
            }
        else
            errorMsg = loginPage.getErrorMsg().getAttribute("innerText");

        try (AutoCloseableSoftAssertions softly = new AutoCloseableSoftAssertions()) {
            softly.assertThat(errorMsg).
                    as("Expected error message \"%s\". But showed \"%s\".", arg1, errorMsg).containsIgnoringCase(arg1);
        }
    }

    @Then("^I should be redirected to the All Blacks Live Streaming portal$")
    public void i_should_be_redirected_to_the_All_Blacks_Live_Streaming_portal() throws Throwable {
        // HACK: Now redirects to not existing page, because i should handly redirect to main page.
        if (!Wait.waitFor(() -> mainPage.isOpen(), shortTimeOut))
            mainPage.openPage();

        String title = "ALL BLACKS";
        String curTitle = mainPage.getTitle();

        try (AutoCloseableSoftAssertions softly = new AutoCloseableSoftAssertions()) {
            softly.assertThat(curTitle).
                    as("The title of page is wrong. Expected: \"%s\". Showed: \"%s\"", title, curTitle).containsIgnoringCase(title);
            softly.assertThat(mainPage.isOpen()).
                    as("Main app page is not open.").isTrue();
        }
    }

    @When("^I goto the NZR ID login page$")
    public void i_goto_the_NZR_ID_login_page() throws Throwable {
        loginPage.openPage();
    }
}
