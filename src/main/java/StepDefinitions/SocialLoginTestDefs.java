package StepDefinitions;

import InteractionObjects.LoginPage;
import InteractionObjects.MainPage;
import InteractionObjects.SocialPages.FaceBookPage;
import InteractionObjects.SocialPages.GooglePage;
import InteractionObjects.SocialPages.TwitterPage;
import Utils.TestSettings;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class SocialLoginTestDefs {
    //@Autowired
    //private World world;
    private LoginPage loginPage = new LoginPage();
    private MainPage mainPage = new MainPage();

    private void loginViaSocialAccount(TestSettings.AccountTypes accountType) {
        TestSettings.Account account = TestSettings.socialAccounts.get(TestSettings.AccountTypes.Google);
        loginPage.openPage();
        switch (accountType) {
            case Facebook:
                loginPage.fbButton.click();
                FaceBookPage fbPage = new FaceBookPage();
                fbPage.login(account.email, account.pass);
                break;
            case Google:
                loginPage.googleButton.click();
                GooglePage googlePage = new GooglePage();
                googlePage.login(account.email, account.pass);
                break;
            case Twitter:
                loginPage.twitterButton.click();
                TwitterPage twitterPage = new TwitterPage();
                twitterPage.login(account.email, account.pass);
                break;
        }
        mainPage.openPage();
        mainPage.logOut();
    }

    @Given("^my Facebook is associated to an registered NZR ID account$")
    public void my_Facebook_is_associated_to_an_registered_NZR_ID_account() throws Throwable {
        loginViaSocialAccount(TestSettings.AccountTypes.Facebook);
    }

    @Given("^my Google is associated to an registered NZR ID account$")
    public void my_Google_is_associated_to_an_registered_NZR_ID_account() throws Throwable {
        loginViaSocialAccount(TestSettings.AccountTypes.Google);
    }

    @Given("^my Twitter is associated to an registered NZR ID account$")
    public void my_Twitter_is_associated_to_an_registered_NZR_ID_account() throws Throwable {
        loginViaSocialAccount(TestSettings.AccountTypes.Twitter);
    }

    @When("^I login using Facebook$")
    public void i_login_using_Facebook() throws Throwable {
        loginPage.fbButton.click();
    }

    @When("^I login using Google$")
    public void i_login_using_Google() throws Throwable {
        loginPage.googleButton.click();
    }

    @When("^I login using Twitter$")
    public void i_login_using_Twitter() throws Throwable {
        loginPage.twitterButton.click();
    }
}
