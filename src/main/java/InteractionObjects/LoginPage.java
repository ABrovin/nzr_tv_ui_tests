package InteractionObjects;

import StepDefinitions.World;
import Utils.BrowserStorage;
import Utils.TestSettings;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.util.List;

import static Utils.TestSettings.accounts;
import static Utils.TestSettings.siteAddress;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;


public class LoginPage extends AbstractPage {

    public String url() {
        return Configuration.baseUrl = siteAddress + "/login";
    }

    public SelenideElement emailInput = $(By.cssSelector("[name='username']"));

    public SelenideElement passInput = $(By.cssSelector("[name='password']"));

    public SelenideElement logInBtn = $(By.cssSelector("button.auth0-lock-submit[type='submit']"));

    //public SelenideElement errorMsg = $(By.cssSelector(".auth0-global-message-error"));

    //public List<SelenideElement> errorPopups = $$(By.cssSelector(".auth0-lock-error-msg"));

    public By notYourAccountLink = By.linkText("Not your account?");

    public By dontRememberYourPassLink = By.linkText("Don't remember your password?");

    private SelenideElement socialButtonsContainer = $(By.cssSelector(".auth0-lock-social-buttons-container"));

    public SelenideElement fbButton = socialButtonsContainer.$(By.cssSelector(".auth0-lock-social-button[data-provider='facebook']"));

    public SelenideElement googleButton = socialButtonsContainer.$(By.cssSelector(".auth0-lock-social-button[data-provider='google-oauth2']"));

    public SelenideElement twitterButton = socialButtonsContainer.$(By.cssSelector(".auth0-lock-social-button[data-provider='twitter']"));

    public SelenideElement getErrorMsg()
    {
        return $(By.cssSelector(".auth0-global-message-error"));
    }

    public List<SelenideElement> getErrorPopups()
    {
        return $$(By.cssSelector(".auth0-lock-error-msg"));
    }

    private void fillLoginData(String login, String pass) {
        emailInput.sendKeys(login);
        passInput.sendKeys(pass);
        logInBtn.click();
    }

    public AbstractPage openPage() {
        super.openPage();

        if (get(dontRememberYourPassLink, 5000) == null)
            get(notYourAccountLink, 5000).click();
        return this;
    }

    public void authenticate(String email) {
        TestSettings.Account account = accounts.get(email);
        authenticate(account.email, account.pass);
    }

    public void authenticate(String email, String pass) {
        String currentUserEmail = BrowserStorage.getCurrentUser();

        if (World.mainPage.isOpen()) {
            // logged other user.
            if (!email.equalsIgnoreCase(currentUserEmail)) {
                World.mainPage.logOut();
                this.openPage();
                this.fillLoginData(email, pass);
            } else // logged right user - refresh page.
                this.refresh();
        } else {
            this.openPage();
            this.fillLoginData(email, pass);
        }
    }
}
