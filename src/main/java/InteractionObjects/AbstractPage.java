package InteractionObjects;

import Utils.Wait;
import com.codeborne.selenide.*;
import org.openqa.selenium.By;

import static Utils.TestSettings.veryShortTimeOut;
import static com.codeborne.selenide.Selenide.$;

public class AbstractPage extends SelenidePageFactory {
    public AbstractPage() {
        //System.setProperty("webdriver.chrome.driver", "D:\\Projects\\Drivers\\chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", "/Users/emily/Automation/nzr_tv_ui_tests/Drivers/chromedriver");
        Configuration.browser = "chrome";
        Configuration.timeout = 6000;
    }

    public String url() {
        return "";
    }

    public AbstractPage openPage() {
        try {
            if (!Wait.waitFor(() -> this.isOpen(), veryShortTimeOut))
                Selenide.open(url());
            else
                Selenide.refresh();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }

    public void refresh() {
        Selenide.refresh();
    }

    public boolean isOpen() {
        if (!WebDriverRunner.url().contains(this.url()))
            return false;
        return true;
    }

    public SelenideElement get(By by, int timeout) {
        try {
            return $(by).waitUntil(Condition.appear, timeout);
        } catch (Throwable t) {
            return null;
        }
    }
/*
    public void clear()
    {
        WebDriverRunner.getWebDriver().manage().deleteAllCookies();
        WebDriverRunner.clearBrowserCache();
    }
    */
}
