package InteractionObjects;

import Utils.BrowserStorage;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.util.List;

import static Utils.TestSettings.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class MainPage extends AbstractPage {
    public String url() {
        //return Configuration.baseUrl = siteAddress + "/live/home";
    	return Configuration.baseUrl = elasticbeanstalkAddress + "/live/home";
        
    }

    //region Elements

    private SelenideElement title = $(By.cssSelector(".navbar-brand"));

    private SelenideElement signBtn = $(By.cssSelector(".sign-button"));

    private List<SelenideElement> gamesList = $$(By.cssSelector(".slick-track[role='listbox'] .match-container"));

    private SelenideElement contact = $(By.cssSelector("a[href='/contact']"));


    //endregion

    //region Getters

    public String getTitle() {
        return title.getAttribute("innerText");
    }

    //endregion

    //region Methods

    public void logOut() {
        if (isOpen() && signBtn.exists())
            signBtn.click();
        else
            BrowserStorage.clear();
    }

    private SelenideElement getGame(String gameTitle) {
        for (SelenideElement game : gamesList) {
            if (game.$(By.cssSelector(".match-title")).getText().equalsIgnoreCase(gameTitle))
                return game;
        }
        return null;
    }

    public void selectGame(String gameTitle) {
        getGame(gameTitle).click();
    }

    public void goToContact() {
        contact.click();
    }

    //endregion
}
