package InteractionObjects;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class MatchInformationPanel {
    //region Elements

    private SelenideElement panel = $(By.cssSelector(".match-information-panel"));

    private SelenideElement cardHolderName = panel.$(By.cssSelector("input#cardholder-name"));
    private SelenideElement cardNumber = panel.$(By.cssSelector("#card-number input"));
    private SelenideElement cardExpiry = panel.$(By.cssSelector("#card-expiry input"));
    private SelenideElement cardCVC = panel.$(By.cssSelector("#card-cvc input"));

    private SelenideElement buyNowBtn = panel.$(By.cssSelector(".button[value='BUY NOW']"));
    private SelenideElement errorMsg = panel.$(By.cssSelector("#payment-form .error"));

    //endregion

    //region Methods

    public void fillPaymentForm(String card_type, String name_on_Card, String card_Number, String card_CVC, String card_Expiry) {
        cardHolderName.sendKeys(name_on_Card);
        cardNumber.sendKeys(card_Number);
        cardExpiry.sendKeys(card_Expiry);
        cardCVC.sendKeys(card_CVC);
    }

    public void clickBuyButton() {
        buyNowBtn.click();
    }

    public String getErrorMessage() {
        return errorMsg.getText();
    }

    //endregion
}
