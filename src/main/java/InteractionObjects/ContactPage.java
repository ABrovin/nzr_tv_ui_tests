package InteractionObjects;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static Utils.TestSettings.siteAddress;
import static com.codeborne.selenide.Selenide.$;
public class ContactPage extends AbstractPage {
    //?basic URL??
	 public String url() {
	        return Configuration.baseUrl = siteAddress + "/contact";
	    }
	//region Elements

    public SelenideElement contactNameFirst = $(By.className("contact-name-first")); 
    private SelenideElement contactNameLast = $(By.className("contact-name-last"));
    private SelenideElement contactEmail = $(By.className("contact-email"));
    private SelenideElement contactSubject = $(By.className("contact-subject"));
    private SelenideElement contactComments = $(By.className("contact-comments"));
  
    private SelenideElement contactSendBtn = $(By.id("contact-send"));
    private SelenideElement contactCancelBtn = $(By.id("contact-cancel"));
    
    private SelenideElement contactAlertSuccess=$(By.cssSelector("div[class='alert success']"));
  //??
    private SelenideElement contactAlertError = $(By.cssSelector("div[class='alert error']"));

    //endregion

    //region Methods
//    public String getContactHeader() {
//        return .get
//    }
    public void fillContactForm(String contact_Firstname, String contact_Lastname, String contact_Email, String contact_Subject) {
    		contactNameFirst.sendKeys(contact_Firstname);
        contactNameLast.sendKeys(contact_Lastname);
        contactEmail.sendKeys(contact_Email);
        contactSubject.sendKeys(contact_Subject);
  
    }
 
    //? shouldn't be type String
    public void fillContactComments(String contact_Comments) {
    	  contactComments.sendKeys(contact_Comments);
    	
    }
  
    public void clickContactSendButton() {
        contactSendBtn.click();
    }
    public void clickContactCancelButton() {
        contactCancelBtn.click();
    }

    public String getAlertSuccess() {
        return contactAlertSuccess.getText();
    }

    public String getAlertError() {
        return contactAlertError.getText();
    }
    //endregion
}

