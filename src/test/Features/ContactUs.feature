Feature: Contact Us

  Background:
# Given I authenticated myself with my "NZR ID"
#   And I visit the "Live Streaming portal"
 Given I authenticated myself with my "john@snow.co.nz"
   # And I visit the "Live Streaming portal"

  @RCL-231 @RCL-232 @assignee:kristyn.lovell @version:MVP @COMPLETED
  Scenario: Show "contact us" form  
    When I go to the "Contact Us" page 
    Then I should see a "GET IN TOUCH WITH US" contact form header
    And I should see the following contact form fields:
      """
      First Name,
      Last Name, 
      Email, 
      Subject, 
      Message
      """
    And the following feature buttons: "Cancel, Send"

 @RCL-231 @RCL-232 @assignee:kristyn.lovell @version:MVP @COMPLETED
  Scenario Outline: Submit "contact us" form

    When I go to the "Contact Us" page 
    And I enter the following infos into the contact form fields
    |contact_firstname  |contact_lastname  |contact_email  |contact_subject  |
     |<contact_firstname>|<contact_lastname>|<contact_email>|<contact_subject>|
    
    And I enter the following message into the contact message formfield:  
      """ 
      Dear NZ Rugby Team,
      
      I got really annoyed because I bought a Live Streaming game on your Allblacks Live website two weeks ago
      but couldn't actually watch it yesterday evening.  When I logged in again, the website told me that 
      I was not allowed to access the content any more. :-(((
      
      Can you please give me a refund for that game?
      
      Thanks,
      John Snow
      
      """
    And I submit the contact form
    Then I should see a success message "Thank you for getting in touch. We'll come back to you as fast as possible."
    And I can go back to the "Live Streaming page" to try again
   
    Examples:
      |contact_firstname  |contact_lastname  |contact_email   |contact_subject  |
      | John              | Snow             | john@snow.co.nz|Refund           |
      
      


  @contact  @RCL-231 @RCL-232 @assignee:kristyn.lovell @version:MVP @COMPLETED
  Scenario Outline: Submit "contact us" form fails

    When I go to the "Contact Us" page 
#    And I enter the following infos into the contact form fields
#    |contact_firstname  |contact_lastname  |contact_email  |contact_subject  |contact_message  |contact_error_message  |
#     |<contact_firstname>|<contact_lastname>|<contact_email>|<contact_subject>|<contact_message>|<contact_error_message>|

	
	And I enter the following infos into the contact form fields
    |contact_firstname  |contact_lastname  |contact_email  |contact_subject  |
     |<contact_firstname>|<contact_lastname>|<contact_email>|<contact_subject>|
    
    And I enter the following message into the contact message formfield:  
      """ 
      Dear NZ Rugby Team,
      
      I got really annoyed because I bought a Live Streaming game on your Allblacks Live website two weeks ago
      but couldn't actually watch it yesterday evening.  When I logged in again, the website told me that 
      I was not allowed to access the content any more. :-(((
      
      Can you please give me a refund for that game?
      
      Thanks,
      John Snow
      
      """
    And I submit the contact form
    Then I should see an error message for contact "<contactform_error_message>"
    
    Examples:
        |contact_firstname  |contact_lastname|contact_email   |contact_subject  |contact_message    |contactform_error_message   |
        |                   | Snow           | john@snow.co.nz|Refund           | Some_example_text |First name can't be blank   |
        | John              |                | john@snow.co.nz|Refund           | Some_example_text |Last name can't be blank    |
        | John              | Snow           |                |Refund           | Some_example_text |Email address can't be blank|
        | John              | Snow           | john@snow.co.nz|                 | Some_example_text |Subject can't be blank      |
 #       | John              | Snow           | john@snow.co.nz|Refund           |                   |Message can't be blank      |
#      
#      
 @RCL-231 @RCL-232 @assignee:kristyn.lovell @version:MVP @COMPLETED
  Scenario: Cancel "Contact us" form

    When I go to the "Contact Us" page 
    	And I enter the following infos into the contact form fields
    |contact_firstname  |contact_lastname  |contact_email  |contact_subject  |
     |<contact_firstname>|<contact_lastname>|<contact_email>|<contact_subject>|
    
    And I enter the following message into the contact message formfield:  
      """ 
      Dear NZ Rugby Team,
      
      I got really annoyed because I bought a Live Streaming game on your Allblacks Live website two weeks ago
      but couldn't actually watch it yesterday evening.  When I logged in again, the website told me that 
      I was not allowed to access the content any more. :-(((
      
      Can you please give me a refund for that game?
      
      Thanks,
      John Snow
      
      """
    #And I (enter/do not enter) infos into the contact form fields
    And I choose cancel 
    Then I should be redirected to the "Live Streaming Homepage"

 @RCL-231 @RCL-232 @assignee:kristyn.lovell @version:MVP @COMPLETED
  Scenario: Submit "contact us" form - system error

    Given I go to the "Contact Us" page 
    And I enter all required contact form fields correctly
    When I submitted the contact form
    And there is a problem sending the message
    Then I should see an error message "There was a problem sending your message. Please try again later."

