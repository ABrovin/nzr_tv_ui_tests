Feature: Purchase games

  In order to watch one or more games, as a viewer, I want to select and purchase the games I am interested in and permitted to view


  Background:
    Given there is a published game 
    And I am eligible to view games in my "region"
    And I authenticated myself with my "john@snow.co.nz"
    And I visit the "Live Streaming portal"

 
  @RCL-213 @RCL-242 @assignee:kristyn.lovell @version:MVP @COMPLETED @logOutBeforeTestStart @logOutAfterTestEnd
  Scenario Outline: Purchase a game (Credit Card valid)

    Given I didn't purchase the game yet 
    And I selected a game
    When I provide the following details of my "<card_type>" in the payment form: <Name_on_Card> <Card_Number> <Card_CVC> <Card_Expiry>
    And I submit the "payment form"
    And My request was processed
    Then I see a "<success_message>"
    And I get a receipt and viewing instructions by email
    And I can go back to the "Live Streaming page"
    
    Examples:
       |Name_on_Card|Card_Number     |Card_CVC|Card_Expiry|card_type           |success_message                            |
       |John Snow   |4242424242424242|123     |10/2019    |Visa                |Success! Your game purchase was successful.|
       |John Snow   |4000056655665556|321     |12/2018    |Visa (debit)        |Success! Your game purchase was successful.|
       |John Snow   |5555555555554444|236     |9/2020     |Mastercard          |Success! Your game purchase was successful.|
       |John Snow   |5200828282828210|845     |8/2029     |Mastercard (debit)  |Success! Your game purchase was successful.|
       |John Snow   |5105105105105100|543     |1/2022     |Mastercard (prepaid)|Success! Your game purchase was successful.|
       |John Snow   |378282246310005 |876     |4/2019     |American Express    |Success! Your game purchase was successful.|
       |John Snow   |6011111111111117|345     |5/2021     |Discover            |Success! Your game purchase was successful.|
       |John Snow   |30569309025904  |345     |3/2020     |Diners Club         |Success! Your game purchase was successful.|
       |John Snow   |3530111333300000|543     |2/2019     |JCB                 |Success! Your game purchase was successful.|
       |            |3530111333300000|543     |2/2019     |JCB                 |Success! Your game purchase was successful.|


  @RCL-213 @RCL-242 @assignee:kristyn.lovell @version:MVP @COMPLETED @test @d
  Scenario Outline: Game Purchase fails (Credit Card Details not valid)

    Given I didn't purchase the game yet
    And I selected a game
    When I provide the following details of my "<card_type>" in the payment form: "<Name_on_Card>" "<Card_Number>" "<Card_CVC>" "<Card_Expiry>"
    And I submit the "payment form"
    And My request was processed
    Then I see an "<error_message>"
    And I can go back to the "Live Streaming page" to try again
    
    Examples:
      |Name_on_Card|Card_Number     |Card_CVC|Card_Expiry|card_type|error_message                                                           |
      |John Snow   |4000000000000002|731     |10/2019    | Visa    |Your card was declined.                                                 |
      |John Snow   |                |731     |10/2019    | Visa    |Your card number is incomplete.                                         |
      |John Snow   |4000000000000127|123     |10/2019    | Visa    |Your card's security code is incorrect.                                 |
      |John Snow   |4000000000000127|        |10/2019    | Visa    |Your card's security code is incomplete.                                |
      |John Snow   |4000000000000069|731     |           | Visa    |Your card's expiration date is incomplete.                              |
      |John Snow   |4000000000000069|731     |02/2017    | Visa    |Your card has expired.                                                  |
      |John Snow   |4000000000000119|731     |12/2019    | Visa    |An error occurred while processing your card. Try again in a little bit.|
      |John Snow   |4242424242424241|731     |12/2019    | Visa    |Your card number is invalid.                                            |