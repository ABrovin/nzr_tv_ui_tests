import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/features",
        glue = "StepDefinitions",
        tags = "@contact",
        dryRun = false,
        strict = false,
        snippets = SnippetType.UNDERSCORE,
        plugin = {"json:target/cucumber.json"})

public class DebugTestRunner {
}